
var MainView = React.createClass({
    render: function () {

        return (
            <div>
                <header>
                    <nav>
                        <ul>
                            <li><a>Stellar Catalogue</a></li>
                        </ul>
                    </nav>
                </header>
                <article>
                    <section id="content">
                        <Search />
                    </section>
                </article>
            </div>
        );
    }
});

var Search = React.createClass({

    getInitialState: function () {
        return { loaded: false };
    },

    search: function (e) {

        var that = this;

        setTimeout(function () {
            that.setState({
                loaded: true,
                data: [{ name: "Andromeda", id: "and" }, { name: "Taurus", id: "tau" }]
            });
        }, 5000);


        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://192.168.13.29:8080" + "/constellations", false);
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);

        this.setState({
            loaded: true,
            data: data
        });


    },

    render: function () {
        return (

            <div>
                <div id="search">
                    <span>Search</span>
                    <div className="controls">
                        <form className="searchform" role="search" method="get" action=".">
                            <div className="surname">
                                <div className="search-container">
                                    <input formControlName="name" id="searchText" autofocus type="search" className="search-input" name="name" value="search" onfocus="if (this.value == 'search') {this.value = '';}" onblur="if (this.value == '') {this.value = 'search';}" ></input>
                                    <button type="button" role="button" value="" className="search-submit" name="submit" title="Search"
                                            onClick={this.search}  ></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <Constellations display={this.state.loaded} data={this.state.data} />
            </div>

        );
    }

});

var Constellations = React.createClass({

    getInitialState: function () {
        return { selected: false };
    },

    select: function(e){
        this.setState({ selected: true });
    },

    render: function () {

        if (!this.props.display) {
            return null;
        }

        var that = this;

        return (

            <div>

                <div id="dbs">
                    <span>Constellations</span>
                    <ul>
                        { this.props.data.map(function (c) {
                            return <li className="db" id={c.id} onClick={that.select}>
                                <span><img src="./img/down.png" /></span><span title=""></span><span>{c.name}</span>
                            </li>;
                        })}
                    </ul>
                </div>
                <Stars display={this.state.selected}/>
            </div>

        );
    }

});

var Stars = React.createClass({

    render: function () {

        if (!this.props.display) {
            return null;
        }

        return (

            <div id="data">
                <span>Stars</span>
                <ul>
                    <li className="db">
                        <span><img src="./img/down.png" /></span><span></span>
                    </li>
                </ul>
            </div>

        );
    }

});


ReactDOM.render(<MainView />, document.getElementById("greeting"));






