
console.log("xyz=" + xyz);

console.log("max=" + findMax(1,5,3,8,2,9,3,1));

function findMax(){
    var max = 0;
    for(var i=0; i<arguments.length; i++){
        if(arguments[i]>max){
            max = arguments[i];
        }
    }
    return max;
}

var myVar = "5";
myVar = new Number(5);


console.log("myVar=" + myVar + " type of " + (typeof myVar));


var o = {
    property1: "value1",
    property2: "value2"
}

o.property3 = "value3";

console.log("p3:" + o.property1)


for(var propertyName in o){
    console.log("name=" + propertyName +", value=" + o[propertyName]);
}

var add = (function () {
    var counter = 0;
    return function () {
        return counter += 1;
    }
})();

var x = add();
x = add();
x = add();

console.log("x= "+ x);

function sum(iks) {
    return function(igrek){
        return iks + igrek;
    }
};

var sum5 = sum(5);

console.log("5+3=" + sum5(3));
