define("starView", function(){
    function StarView() {
    }

    StarView.prototype.render = function (el, data) {
        var items = "";
        for (var i = 0, len = data.length; i < len; i++) {
            var c = data[i];
            items = items +
                '<li class="db" id="' + c.id + '" >' +
                '<span><img src="http://www.jmkxyy.com/4-star-icon/4-star-icon-10.jpg" /></span><span title=""></span><span>' + c.name + '</span>' +
                '</li>';
        }
        document.getElementById(el).innerHTML =
            '<div id="dbs">' +
            '<span>Stars</span>' +
            '<ul>' +
            items +
            '</ul>' +
            '</div>' +
            '<Stars display={this.state.selected}/>';
    }

    return StarView;
});