

requirejs.config({
    baseUrl: 'js'
});

var backendServer = "http://192.168.13.29:8080";

/*
var MainView = require(["mainView"]);

var ConstellationView = require(["constellationView"]);

var StarView = require(["starView"]);
*/

require(["mainView", "constellationView", "starView"], function(MainView, ConstellationView, StarView) {
    var starView = new StarView();
    var constellationView = new ConstellationView(starView);
    var mainView = new MainView(constellationView);


    window.actionSearch = function(){mainView.search();}; //mainView.search.bind(mainView);
    window.actionStars = function(id){constellationView.searchStars(id);};

    mainView.render("greeting");
});








