define("mainView", function(){
    function MainView(constellationView){
        this.constellationView = constellationView;
        //this.constellationView = arguments[0];
    }

    MainView.prototype.render = function(el) {
        document.getElementById(el).innerHTML= '<div>' +
            '<header>' +
            '<nav>' +
            '<ul>' +
            '<li><a>Stellar Catalogue</a></li>' +
            '</ul>' +
            '</nav>' +
            '</header>' +
            '<article>' +
            '<section id="content">' +
            '<div>\n' +
            '<div id="search">\n' +
            '<span>Search</span>' +
            '<div class="controls">' +
            '<form class="searchform" role="search" method="get" action=".">' +
            '<div class="surname">' +
            '<div class="search-container">' +
            '<input formControlName="name" id="searchText" autofocus type="search" class="search-input" name="name" value="search" ></input>' +
            '<button type="button" role="button" value="" class="search-submit" name="submit" title="Search"\n' +
            'onClick="actionSearch(this)"  ></button>' +
            '</div>\n' +
            '</div>\n' +
            '</form>\n' +
            '</div>      \n' +
            '</div>\n' +
            '<div id="constellations"/>\n' +
            '</div>'
        '</section>' +
        '</article>' +
        '</div>';
    }

    MainView.prototype.search = function() {

        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", backendServer + "/constellations", false);
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);

        //var data = [{ name: "Andromeda", id: "and" }, { name: "Lynx", id: "lyn" }, { name: "Taurus", id: "tau" }];
        this.constellationView.render("constellations", data);

    }

    return MainView;
});