define("constellationView", function(){

    function ConstellationView(starView) {
        this.starView = starView;
    }

    ConstellationView.prototype.render = function (el, data) {
        var items = "";

        for(var i=0, len=data.length; i < len; i++){
            var c = data[i];
            items = items +
                //'<li class="db" id="' + c.id + '" onClick="actionStars("'+ c.id + '")>' +
                '<li class="db" id="' + c.id + '" onClick="actionStars(\''+ c.id +'\')">' +
                '<span><img src="http://www.jmkxyy.com/4-star-icon/4-star-icon-10.jpg" /></span><span title=""></span><span>' + c.name + '</span>' +
                '</li>';
        }
        document.getElementById(el).innerHTML =
            '<div id="dbs">' +
            '<span>Constellations</span>' +
            '<ul>' +
            items +
            '</ul>' +
            '</div>' +
            '<div id="stars"/>';
    }

    ConstellationView.prototype.searchStars = function(id) {

        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", backendServer + "/constellations/" + id + "/stars", false);
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);
        //var data = [];
        this.starView.render("stars", data);
    }

    return ConstellationView;
});